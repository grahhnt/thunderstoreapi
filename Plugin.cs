﻿using System;
using System.Collections.Generic;
using System.IO;
using BepInEx;
using BepInEx.Logging;
using YamlDotNet.RepresentationModel;
using YamlDotNet.Serialization;

namespace ThunderstoreAPI
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    [ContentWarningPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, true)]
    public class Plugin : BaseUnityPlugin
    {
        internal new static ManualLogSource Logger;

        private void Awake()
        {
            Logger = base.Logger;

            Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} is loaded!");
        }
    }

    /**
     * From r2mm's mods.yml in the profile folder
     */
    public class ThunderstoreMod
    {
        public int manifestVersion;
        public string name;
        public string authorName;
        public string websiteUrl;
        public string displayName;
        public string description;
        public string gameVersion;
        public string networkMode;
        public string packageType;
        public string installMode;
        public object[] loaders;
        public string[] dependencies;
        public string[] optionalDependencies;
        public string[] incompatibilities;
        public Version versionNumber;
        public bool enabled;
        public string icon;

        public class Version
        {
            public int major;
            public int minor;
            public int patch;
        }
    }

    /**
     * The current manifest Thunderstore uses
     */
    public class ThunderstoreManifest
    {
        public string name;
        public string description;
        public string version_number;
        public string[] dependencies;
        public string website_url;
    }

    /**
     * This is used when importing a local mod
     * 
     * Possibly related: https://github.com/thunderstore-io/Thunderstore/issues/163
     */
    public class ThunderstoreManifestV2
    {
        public int manifestVersion;
        public string name;
        public string authorName;
        public string websiteUrl;
        public string displayName;
        public string description;
        public string gameVersion;
        public string networkMode;
        public string packageType;
        public string installMode;
        public object[] loaders;
        public object[] dependencies;
        public object[] incompatibilities;
        public object[] optionalDependencies;
        public Version version;
        public bool enabled;
        public string icon;

        public class Version
        {
            public int major;
            public int minor;
            public int patch;
        }
    }

    /**
    * API to get plugins loaded from the Thunderstore launcher / R2MM
    */
    public class Library
    {
        /**
         * Get every mod registered for this profile
         */
        public static List<ThunderstoreMod> GetMods()
        {
            var modsYML = Path.Combine(Paths.PluginPath, "..", "..", "mods.yml");
            if (!File.Exists(modsYML))
            {
                Plugin.Logger.LogWarning("Unable to find mods.yml");
                return [];
            }

            using var reader = new StreamReader(modsYML);
            var deserializer = new DeserializerBuilder().IgnoreUnmatchedProperties().Build();
            var mods = deserializer.Deserialize<ThunderstoreMod[]>(reader);

            return new List<ThunderstoreMod>(mods);
        }

        /**
         * Get all loaded mods
         */
        public static List<ThunderstoreMod> GetLoadedMods()
        {
            List<ThunderstoreMod> mods = [];
            var _mods = GetMods();

            foreach (var mod in _mods)
            {
                if (mod.enabled)
                {
                    mods.Add(mod);
                }
            }

            return mods;
        }
    }
}

