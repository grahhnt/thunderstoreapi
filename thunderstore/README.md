# ThunderstoreAPI

A simple library to read r2mm's/Thunderstore's mods.yml from the profile folder

## Developer Usage

- Add this mod as a dependency to your mod
- Make sure to add ThunderstoreAPI.dll to your project references

### `ThunderstoreAPI.Library.GetLoadedMods()`

Returns `List<ThunderstoreMod>` containing all of the loaded/enabled mods

### `ThunderstoreAPI.Library.GetMods()`

Returns `List<ThunderstoreMod>` containing _all_ of the mods in profile (not recommended)

### class `ThunderstoreMod`

| Property             | Type     | Description                                  |
| -------------------- | -------- | -------------------------------------------- |
| manifestVersion      | int      |                                              |
| name                 | string   | Thunderstore Mod ID (`sc07-ThunderstoreAPI`) |
| authorName           | string   | Thunderstore team name (`sc07`)              |
| websiteUrl           | string   | Thunderstore mod page                        |
| displayName          | string   | Mod display name                             |
| description          | string   | Mod description                              |
| gameVersion          | string   |                                              |
| networkMode          | string   |                                              |
| packageType          | string   |                                              |
| installMode          | string   |                                              |
| loaders              | object[] |                                              |
| dependencies         | string[] | Thunderstore mod ID list, including version  |
| optionalDependencies | string[] | ^^                                           |
| incompatibilities    | string[] | ^^                                           |
| versionNumber        | Version  | Mod version                                  |
| enabled              | bool     |                                              |
| icon                 | string   | Path to icon                                 |

### class `ThunderstoreMod.Version`

| Property | Type |
| -------- | ---- |
| major    | int  |
| minor    | int  |
| patch    | int  |
